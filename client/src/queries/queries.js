import { gql } from 'apollo-boost';

const getBooksQuery = gql`
  {
    books {
      title
      id
      author {
        firstName
        lastName
      }
    }
  }
`;

const getBookQuery = gql`
  query($id: ID) {
    book(id: $id) {
      id
      title
      genre
      publishDate
      author {
        id
        firstName
        lastName
        dob
        books {
          title
          id
        }
      }
    }
  }
`;

const getAuthorsQuery = gql`
  {
    authors {
      firstName
      lastName
      id
      books {
        title
        genre
        id
      }
    }
  }
`;

const addBookMutation = gql`
  mutation($title: String!, $genre: String!, $authorId: [ID!]!) {
    addBook(title: $title, genre: $genre, authorId: $authorId) {
      title
      id
    }
  }
`;

const addAuthorMutation = gql`
  mutation($firstName: String!, $lastName: String!, $dob: String!) {
    addAuthor(firstName: $firstName, lastName: $lastName, dob: $dob) {
      firstName
      lastName
      id
    }
  }
`;

export {
  getAuthorsQuery,
  getBooksQuery,
  addBookMutation,
  getBookQuery,
  addAuthorMutation
};
