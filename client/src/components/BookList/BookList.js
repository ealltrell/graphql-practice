import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import { getBooksQuery } from '../../queries/queries';

import BookDetails from '../BookDetails/BookDetails';

export class BookList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: null
    };
  }
  displayBooks() {
    let data = this.props.data;
    if (data.loading) {
      return <div>Loading books...</div>;
    } else {
      return data.books.map(book => {
        let authors = book.author
          .map(author => {
            return author.firstName + ' ' + author.lastName;
          })
          .join(', ');
        console.log(authors);
        return (
          <li
            key={book.id}
            onClick={e => {
              this.setState({ selected: book.id });
            }}
          >
            {book.title} - {authors}
          </li>
        );
      });
    }
  }
  showClearSelectionButton() {
    if (this.state.selected) {
      return (
        <button type="button" onClick={e => this.setState({ selected: null })}>
          Clear selection
        </button>
      );
    }
  }
  render() {
    return (
      <div>
        <h1>Book List</h1>
        <ul id="book-list">{this.displayBooks()}</ul>
        {this.showClearSelectionButton()}
        <BookDetails bookId={this.state.selected} />
      </div>
    );
  }
}

export default graphql(getBooksQuery)(BookList);
