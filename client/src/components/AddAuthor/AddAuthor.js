import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import { addAuthorMutation, getAuthorsQuery } from '../../queries/queries';

class AddAuthor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      dob: ''
    };
  }
  submitForm(e) {
    e.preventDefault();
    let newAuthor = {
      variables: {
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        dob: this.state.dob
      },
      refetchQueries: [{ query: getAuthorsQuery }]
    };
    this.props.addAuthorMutation(newAuthor);
    this.setState({
      firstName: '',
      lastName: '',
      dob: ''
    });
  }
  render() {
    return (
      <form id="add-Book" onSubmit={this.submitForm.bind(this)}>
        <h3>Add Author</h3>
        <div className="field">
          <label>Author first name: </label>
          <input
            type="text"
            onChange={e => this.setState({ firstName: e.target.value })}
            value={this.state.firstName}
          />
        </div>

        <div className="field">
          <label>Author last name: </label>
          <input
            type="text"
            onChange={e => this.setState({ lastName: e.target.value })}
            value={this.state.lastName}
          />
        </div>

        <div className="field">
          <label>DOB: </label>
          <input
            type="date"
            name="dob"
            id="author-dob"
            onChange={e => this.setState({ dob: e.target.value })}
            value={this.state.dob}
          />
        </div>

        <button>Add</button>
      </form>
    );
  }
}

export default graphql(addAuthorMutation, { name: 'addAuthorMutation' })(
  AddAuthor
);
