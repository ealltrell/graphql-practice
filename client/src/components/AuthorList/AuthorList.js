import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import { getAuthorsQuery } from '../../queries/queries';

export class AuthorList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: null
    };
  }
  displayAuthors() {
    let data = this.props.data;
    if (data.loading) {
      return <div>Loading authors...</div>;
    } else {
      return data.authors.map(author => {
        let books = author.books
          .map(book => {
            return book.title;
          })
          .join(', ');
        return (
          <li
            key={author.id}
            onClick={e => {
              this.setState({ selected: author.id });
            }}
          >
            {author.firstName + ' ' + author.lastName} - {books}
          </li>
        );
      });
    }
  }
  render() {
    return (
      <div>
        <h1>Author List</h1>
        <ul id="book-list">{this.displayAuthors()}</ul>
      </div>
    );
  }
}

export default graphql(getAuthorsQuery)(AuthorList);
