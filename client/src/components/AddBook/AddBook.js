import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';
import {
  getAuthorsQuery,
  addBookMutation,
  getBooksQuery
} from '../../queries/queries';

class AddBook extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      genre: '',
      authorId: ''
    };
  }
  displayAuthors() {
    let data = this.props.getAuthorsQuery;
    if (data.loading) {
      return <option>Loading Authors</option>;
    } else {
      return data.authors.map(author => {
        return (
          <option key={author.id} value={author.id}>
            {author.firstName} {author.lastName}
          </option>
        );
      });
    }
  }
  submitForm(e) {
    e.preventDefault();
    let newBook = {
      variables: {
        title: this.state.title,
        genre: this.state.genre,
        authorId: [this.state.authorId]
      },
      refetchQueries: [{ query: getBooksQuery }, { query: getAuthorsQuery }]
    };
    console.log(this.state);
    this.props.addBookMutation(newBook);
    this.setState({
      title: '',
      genre: '',
      authorId: ''
    });
  }
  render() {
    return (
      <form id="add-Book" onSubmit={this.submitForm.bind(this)}>
        <h3>Add Book</h3>
        <div className="field">
          <label>Book title: </label>
          <input
            type="text"
            onChange={e => this.setState({ title: e.target.value })}
            value={this.state.title}
          />
        </div>

        <div className="field">
          <label>Genre: </label>
          <input
            type="text"
            onChange={e => this.setState({ genre: e.target.value })}
            value={this.state.genre}
          />
        </div>

        <div className="field">
          <label>Author: </label>
          <select
            onChange={e => this.setState({ authorId: e.target.value })}
            value={this.state.authorId}
          >
            <option>Select author</option>
            {this.displayAuthors()}
          </select>
        </div>

        <button>Add</button>
      </form>
    );
  }
}

export default compose(
  graphql(getAuthorsQuery, { name: 'getAuthorsQuery' }),
  graphql(addBookMutation, { name: 'addBookMutation' })
)(AddBook);
