// Vendor
import React, { Component } from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

// Style
import './App.css';

// Components
import BookList from './components/BookList/BookList';
import AddBook from './components/AddBook/AddBook';
import AuthorList from './components/AuthorList/AuthorList';
import AddAuthor from './components/AddAuthor/AddAuthor';

// Apollo client setup
const client = new ApolloClient({
  uri: 'http://localhost:1337/graphql'
});

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <div className="App">
          <BookList />
          <AuthorList />
          <AddBook />
          <AddAuthor />
        </div>
      </ApolloProvider>
    );
  }
}

export default App;
