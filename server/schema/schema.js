const _ = require('lodash');
const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLID,
  GraphQLSchema,
  GraphQLList
} = require('graphql');
const Book = require('../models/book');
const Author = require('../models/author');

const BookType = new GraphQLObjectType({
  name: 'Book',
  fields: () => ({
    id: { type: GraphQLID },
    title: { type: GraphQLString },
    genre: { type: GraphQLString },
    publishDate: { type: GraphQLString },
    author: {
      type: new GraphQLList(AuthorType),
      resolve(parent, args) {
        return Author.find({}).then(authors => {
          return authors.filter(author => {
            return parent.authorId.includes(String(author._id));
          });
        });
      }
    }
  })
});

const AuthorType = new GraphQLObjectType({
  name: 'Author',
  fields: () => ({
    id: { type: GraphQLID },
    firstName: { type: GraphQLString },
    lastName: { type: GraphQLString },
    dob: { type: GraphQLString },
    books: {
      type: new GraphQLList(BookType),
      resolve(parent, args) {
        return Book.find({ authorId: parent.id });
      }
    }
  })
});

const RootQuery = new GraphQLObjectType({
  name: 'Query',
  fields: {
    author: {
      type: AuthorType,
      args: {
        id: { type: GraphQLID }
      },
      resolve(root, { id }, context, info) {
        console.log(context);
        return Author.findOne({ _id: id });
      }
    },
    book: {
      type: BookType,
      args: {
        id: { type: GraphQLID }
      },
      resolve(root, { id }) {
        return Book.findById(id);
      }
    },
    books: {
      type: new GraphQLList(BookType),
      resolve(root, args) {
        return Book.find({});
      }
    },
    authors: {
      type: new GraphQLList(AuthorType),
      resolve(root, args) {
        return Author.find({});
      }
    }
  }
});

const Mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    addAuthor: {
      type: AuthorType,
      args: {
        id: { type: GraphQLID },
        firstName: { type: GraphQLString },
        lastName: { type: GraphQLString },
        dob: { type: GraphQLString }
      },
      resolve(parent, args) {
        let author = new Author({
          id: args.id,
          firstName: args.firstName,
          lastName: args.lastName,
          dob: args.dob
        });
        return author.save();
      }
    },
    addBook: {
      type: BookType,
      args: {
        id: { type: GraphQLID },
        title: { type: GraphQLString },
        genre: { type: GraphQLString },
        authorId: { type: new GraphQLList(GraphQLID) },
        publishDate: { type: GraphQLString }
      },
      resolve(parent, args) {
        let book = new Book({
          id: args.id,
          title: args.title,
          genre: args.genre,
          authorId: args.authorId,
          publishDate: args.publishDate
        });
        return book.save();
      }
    }
  }
});

const Schema = new GraphQLSchema({
  query: RootQuery,
  mutation: Mutation
});

module.exports = Schema;
