const express = require('express');
const graphqlHTTP = require('express-graphql');
const schema = require('./schema/schema');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();

// Allow cross origin requests
app.use(cors());

// Requires local instance of MongoDB to be active
// mongoose.connect(
//   'mongodb://localhost:27017/GRAPHQL_SERVER_DB',
//   { useNewUrlParser: true }
// );
mongoose.connect(
  'mongodb://ealltrell:baabaa1234@ds143000.mlab.com:43000/coolbox-gql',
  { useNewUrlParser: true }
);
mongoose.connection.once('open', () => {
  console.log('connected to mongoDB database');
});

// For GraphQL server
app.use(
  '/graphql',
  graphqlHTTP({
    schema,
    graphiql: true
  })
);

app.listen(1337);
console.log('GraphiQL running at http://localhost:1337/graphql');
